package controllers;

import com.avaje.ebean.Ebean;
import models.Application;
import models.BinaryData;
import models.Developer;
import play.data.Form;
import play.data.validation.ValidationError;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.submissions.binariesForm;
import views.html.submissions.metadataForm;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import static controllers.Applications.GO_HOME;
import static controllers.Authentication.developerID;
import static org.apache.commons.lang3.StringUtils.contains;
import static org.apache.commons.lang3.StringUtils.split;
import static org.springframework.util.StringUtils.removeDuplicateStrings;
import static play.mvc.Http.MultipartFormData;
import static play.mvc.Http.MultipartFormData.FilePart;

@Security.Authenticated(Secured.class)
public class Submissions extends Controller {
  public static final int MEGABYTE = 1048576;

  public static Result startNew() {
    int unsubmittedID = Application.findUnsubmitted(developerID());
    if (unsubmittedID != 0) {
      return redirect(
        routes.Submissions.startUpload(unsubmittedID)
      );
    }

    Form<Application> applicationForm = form(Application.class).fill(
      new Application() {{
        version = 1;
      }}
    );

    return ok(
      metadataForm.render(
        applicationForm,
        Developer.find(developerID())
      )
    );
  }

  public static Result startNewVersion(int previousApplicationID) {
    if (!Secured.isOwnerOf(previousApplicationID)) {
      return unauthorized(views.html.defaultpages.unauthorized.render());
    }

    Application previous = Application.find(previousApplicationID);
    previous.version++;
    previous.versionDisplay = "";
    previous.keywords = Application.findKeywords(previous.id);
    previous.previousVersionID = previous.id;

    Form<Application> applicationForm = form(Application.class).fill(previous);

    return ok(
      metadataForm.render(
        applicationForm,
        Developer.find(developerID())
      )
    );
  }

  public static Result saveNewVersion() throws SQLException, IOException {
    return save();
  }

  public static Result save() throws SQLException, IOException {
    Form<Application> applicationForm = form(Application.class).bindFromRequest();

    reRejectGlobally(applicationForm, "name", "Please choose a name of at least 3 and not more than 35 characters long.");
    reRejectGlobally(applicationForm, "versionDisplay", "Please use a typical software versioning convention (e.g., 1.0 or 1.0.1).");
    reRejectGlobally(applicationForm, "keywords", "Please use space delimited lowercase keywords of at least 3 and not more than 15 letters long (e.g., adventure medieval novel). Provide at least 3 and not more than 7 keywords.");
    rejectTakenName(applicationForm);
    rejectDuplicateKeywords(applicationForm);

    if (applicationForm.hasErrors()) {
      return badRequest(
        metadataForm.render(
          applicationForm,
          Developer.find(developerID())
        )
      );
    }

    int applicationID = applicationForm.get().save(developerID());

    flash("success", "Metadata of the application '" + applicationForm.get().name + "' has been saved.");
    return redirect(
      routes.Submissions.startUpload(applicationID)
    );
  }

  private static void reRejectGlobally(Form<Application> form, String field, String message) {
    ValidationError error = form.error(field);
    if (error != null && !"error.required".equals(error.message()))
      form.reject(message);
  }

  private static void rejectTakenName(Form<Application> applicationForm) {
    if (applicationForm.error("name") == null && "0".equals(applicationForm.field("previousVersionID").value())) {
      String name = applicationForm.field("name").value();
      if (Application.isNameTaken(name)) {
        applicationForm.reject("name", "error");
        applicationForm.reject("The name '" + name + "' is already taken. Please choose another name.");
      }
    }
  }

  private static void rejectDuplicateKeywords(Form<Application> applicationForm) {
    if (applicationForm.error("keywords") == null && removeDuplicateStrings(split(applicationForm.field("keywords").value())).length < 3) {
      applicationForm.reject("keywords", "error");
      applicationForm.reject("Please provide at least 3 unique keywords.");
    }
  }

  public static Result startUpload(int applicationID) {
    Form<BinaryData> binaryDataForm = form(BinaryData.class);
    return ok(
      binariesForm.render(
        binaryDataForm,
        applicationID,
        Developer.find(developerID())
      )
    );
  }

  public static Result submit() throws IOException {
    Form<BinaryData> binaryDataForm = form(BinaryData.class).bindFromRequest();

    BinaryData binaryData = extractBinaryData(binaryDataForm);

    if (binaryDataForm.hasErrors()) {
      binaryDataForm.reject("Please provide all binaries and verify that they are of an appropriate format (.app or .bin for the binary and .jpg, .png or .bmp for images). Maximum binary size is 3 MB.");
      return badRequest(
        binariesForm.render(
          binaryDataForm,
          binaryData.applicationID,
          Developer.find(developerID())
        )
      );
    }

    Ebean.beginTransaction();
    try {
      binaryData.save();
      Application.submit(binaryData.applicationID, developerID());
      Ebean.commitTransaction();
    }
    finally {
      Ebean.endTransaction();
    }

    flash("success", "The application '" + Application.findName(binaryData.applicationID) + "' has been submitted.");
    return GO_HOME;
  }

  static BinaryData extractBinaryData(Form<BinaryData> form) {
    MultipartFormData body = request().body().asMultipartFormData();
    BinaryData binaryData = form.get();

    binaryData.binary = extractFile(form, body, "binary", "application");
    binaryData.artwork = extractFile(form, body, "artwork", "image");
    binaryData.icon = extractFile(form, body, "icon", "image");
    binaryData.screenshot1 = extractFile(form, body, "screenshot1", "image");
    binaryData.screenshot2 = extractFile(form, body, "screenshot2", "image");
    binaryData.screenshot3 = extractFile(form, body, "screenshot3", "image");
    binaryData.screenshot4 = extractFile(form, body, "screenshot4", "image");
    binaryData.screenshot5 = extractFile(form, body, "screenshot5", "image");

    return binaryData;
  }

  private static File extractFile(Form<BinaryData> form, MultipartFormData body, String field, String type) {
    FilePart filePart = body.getFile(field);

    if (filePart != null && contains(filePart.getContentType(), type)) {
      File file = filePart.getFile();
      if (file.length() < 3 * MEGABYTE)
        return file;
    }

    form.reject(field, "error");
    return null;
  }
}
