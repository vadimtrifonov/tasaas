package controllers;

import models.Application;
import models.BinaryData;
import models.Developer;
import models.Submission;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.applications.application;
import views.html.applications.list;
import views.html.uploadForm;

import java.io.IOException;
import java.sql.SQLException;

import static controllers.Authentication.developerID;

@Security.Authenticated(Secured.class)
public class Applications extends Controller {

  public static Result GO_HOME = redirect(
    routes.Applications.list(0, "name", "asc")
  );

  public static Result index() {
    return GO_HOME;
  }

  public static Result list(int pageIndex, String sortBy, String order) throws SQLException {
    return ok(
      list.render(
        Application.page(developerID(), pageIndex, 5, sortBy, order),
        sortBy,
        order,
        Developer.find(developerID())
      )
    );
  }

  public static Result open(int applicationID) throws SQLException {
    if (!Secured.isOwnerOf(applicationID)) {
      return unauthorized(views.html.defaultpages.unauthorized.render());
    }

    return ok(
      application.render(
        Application.find(applicationID),
        Submission.list(applicationID),
        Developer.find(developerID())
      )
    );
  }

  public static Result artwork(int applicationID) throws SQLException {
    return ok(BinaryData.findArtwork(applicationID));
  }

  //----------------------------------------------------------------------------------------------------------------------

  public static Result binaries() throws IOException {
    Form<BinaryData> binaryDataForm = form(BinaryData.class);
    return ok(uploadForm.render(binaryDataForm, Developer.find(developerID())));
  }

  public static Result upload() throws IOException, SQLException {
    Form<BinaryData> binaryDataForm = form(BinaryData.class).bindFromRequest();

    BinaryData binaryData = Submissions.extractBinaryData(binaryDataForm);

    if (binaryDataForm.hasErrors()) {
      binaryDataForm.reject("Wrong binaries");
      return badRequest(uploadForm.render(binaryDataForm, Developer.find(developerID())));
    }

    binaryData.save();

    flash("success", "Binaries for application '" + Application.findName(binaryData.applicationID) + " (ID: " + binaryData.applicationID + ")' have been uploaded.");
    return GO_HOME;
  }
}
