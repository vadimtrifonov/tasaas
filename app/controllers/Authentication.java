package controllers;

import models.Developer;
import play.Logger;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.login;

import static controllers.Applications.GO_HOME;

public class Authentication extends Controller {

  public static class Login {
    public String email;
    public String passphrase;

    public String validate() {
      try {
        session("developer_id", (Developer.authenticate(email, passphrase)).toString());
        return null;
      }
      catch (RuntimeException e) {
        Logger.info(email + "?" + passphrase + ": " + e.getMessage());
        return "Invalid email or passphrase";
      }
    }
  }

  public static Result login() {
    return ok(
      login.render(form(Login.class))
    );
  }

  public static Result authenticate() {
    Form<Login> loginForm = form(Login.class).bindFromRequest();

    if (loginForm.hasErrors()) {
      return badRequest(
        login.render(loginForm)
      );
    }
    return GO_HOME;
  }

  public static Result logout() {
    session().clear();
    flash("success", "You have been signed out");
    return redirect(
      routes.Authentication.login()
    );
  }

  public static int developerID() {
    return Integer.valueOf(request().username());
  }
}