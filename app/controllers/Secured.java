package controllers;

import models.Application;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Results;
import play.mvc.Security;

import static controllers.Authentication.developerID;

public class Secured extends Security.Authenticator {

  @Override
  public String getUsername(Context context) {
    return context.session().get("developer_id");
  }

  @Override
  public Result onUnauthorized(Context context) {
    if (context.session().isEmpty()) {
      return Results.redirect(routes.Authentication.login());
    }
    return unauthorized(views.html.defaultpages.unauthorized.render());
  }

  public static boolean isOwnerOf(int applicationID) {
    return Application.isOwner(applicationID, developerID());
  }
}