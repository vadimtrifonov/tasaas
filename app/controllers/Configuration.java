package controllers;

import play.Play;

public class Configuration {
  public static final String DB_SCHEMA = (Play.application().isProd())
    ? Play.application().configuration().getString("pg.db.schema")
    : Play.application().configuration().getString("pg.db.dev.schema");
}
