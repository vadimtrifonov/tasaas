package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.annotation.Sql;
import models.classifiers.SubmissionState;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;
import java.util.List;

import static controllers.Configuration.DB_SCHEMA;

@Entity
@Sql
public class Submission {
  public int id;
  public Date submitted;
  public Date resolved;
  @ManyToOne
  public SubmissionState state;

  public static List<Submission> list(int applicationID) {
    RawSql rawSql = RawSqlBuilder.unparsed("SELECT * FROM " + DB_SCHEMA + ".f_submission_list(p_application_id:= ?)")
      .columnMapping("sub_id", "id")
      .columnMapping("created_date_time", "submitted")
      .columnMapping("closed_date_time", "resolved")
      .columnMapping("sub_state_id", "state.id")
      .columnMapping("sub_state_name", "state.name")
      .create();

    return Ebean.find(Submission.class)
      .setRawSql(rawSql)
      .setParameter(1, applicationID)
      .findList();
  }
}
