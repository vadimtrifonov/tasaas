package models;

import com.avaje.ebean.CallableSql;
import com.avaje.ebean.Ebean;

import java.io.File;
import java.io.IOException;
import java.sql.Types;

import static com.google.common.io.Files.toByteArray;
import static controllers.Configuration.DB_SCHEMA;

public class BinaryData {
  public int applicationID;
  public File binary;
  public File artwork;
  public File icon;
  public File screenshot1;
  public File screenshot2;
  public File screenshot3;
  public File screenshot4;
  public File screenshot5;

  public void save() throws IOException {
    CallableSql cs = Ebean.createCallableSql("{call " + DB_SCHEMA + ".f_binary_datum_new(?, ?, ?, ?, ?, ?, ?, ?, ?)}")
      .addModification(DB_SCHEMA + ".binary_datum", true, false, false)
      .addModification(DB_SCHEMA + ".screenshot", true, false, false)
      .setParameter(1, applicationID)
      .setParameter(2, toByteArray(binary))
      .setParameter(3, toByteArray(artwork))
      .setParameter(4, toByteArray(icon))
      .setParameter(5, toByteArray(screenshot1))
      .setParameter(6, toByteArray(screenshot2))
      .setParameter(7, toByteArray(screenshot3))
      .setParameter(8, toByteArray(screenshot4))
      .setParameter(9, toByteArray(screenshot5));

    Ebean.execute(cs);
  }

  public static byte[] findArtwork(int applicationID) {
    CallableSql cs = Ebean.createCallableSql("{call " + DB_SCHEMA + ".f_binary_datum_find_artwork(?, ?)}")
      .setParameter(1, applicationID)
      .registerOut(2, Types.LONGVARBINARY);

    Ebean.execute(cs);

    return (byte[]) cs.getObject(2);
  }
}
