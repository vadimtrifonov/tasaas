package models;

import com.avaje.ebean.CallableSql;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.annotation.Sql;
import models.classifiers.ApplicationState;
import models.classifiers.Category;
import models.classifiers.ContentRating;
import models.classifiers.PriceTier;
import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Pattern;
import play.data.validation.Constraints.Required;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import static controllers.Configuration.DB_SCHEMA;
import static play.data.validation.Constraints.MaxLength;

@Entity
@Sql
public class Application {
  public static final String NO_LEADING_TRAILING_SPACES = "^\\S.*\\S$";
  public static final String DOTALL_NOT_BLANK = "(?s)^.*\\S.*$";
  public static final String VERSIONING_CONVENTION = "^([0-9][.][0-9]){1}([.][0-9])?$";
  public static final String LOWERCASE_SPACE_DELIMITED_3TO15_MIN3_MAX7 = "^[a-z]{3,15}( [a-z]{3,15}){2,6}$";

  public int id;
  @Required @Pattern(NO_LEADING_TRAILING_SPACES) @MinLength(3) @MaxLength(35)
  public String name;
  public int version;
  @Required @Pattern(VERSIONING_CONVENTION)
  public String versionDisplay;
  @Pattern(DOTALL_NOT_BLANK)
  public String noteToReviewer;
  @Required @Pattern(DOTALL_NOT_BLANK)
  public String description;
  @Pattern(NO_LEADING_TRAILING_SPACES) @MaxLength(50)
  public String website;
  @Required @Email @MaxLength(50)
  public String supportEmail;
  @Required @Pattern(LOWERCASE_SPACE_DELIMITED_3TO15_MIN3_MAX7)
  public String keywords;
  @ManyToOne @Valid
  public Category category;
  @ManyToOne @Valid
  public ContentRating contentRating;
  @ManyToOne @Valid
  public PriceTier priceTier;
  @ManyToOne
  public ApplicationState state;
  public int previousVersionID;

  public static Application find(int applicationID) {
    RawSql rawSql = RawSqlBuilder.unparsed("SELECT * FROM " + DB_SCHEMA + ".f_application_find(p_application_id:= ?)")
      .columnMapping("id", "id")
      .columnMapping("name", "name")
      .columnMapping("version", "version")
      .columnMapping("version_display", "versionDisplay")
      .columnMapping("note_to_reviewer", "noteToReviewer")
      .columnMapping("description", "description")
      .columnMapping("website", "website")
      .columnMapping("support_email", "supportEmail")
      .columnMapping("category_id", "category.id")
      .columnMapping("category_name", "category.name")
      .columnMapping("rating_id", "contentRating.id")
      .columnMapping("rating_name", "contentRating.name")
      .columnMapping("tier_id", "priceTier.id")
      .columnMapping("tier_name", "priceTier.name")
      .columnMapping("state_id", "state.id")
      .columnMapping("state_name", "state.name")
      .create();

    return Ebean.find(Application.class)
      .setRawSql(rawSql)
      .setParameter(1, applicationID)
      .findUnique();
  }

  public int save(int developerID) {
    CallableSql cs = Ebean.createCallableSql("{call " + DB_SCHEMA + ".f_application_new(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}")
      .addModification(DB_SCHEMA + ".application", true, false, false)
      .addModification(DB_SCHEMA + ".keyword", true, false, false)
      .addModification(DB_SCHEMA + ".application_keyword", true, false, false)
      .setParameter(1, developerID)
      .setParameter(2, name)
      .setParameter(3, version)
      .setParameter(4, versionDisplay)
      .setParameter(5, noteToReviewer)
      .setParameter(6, description)
      .setParameter(7, website)
      .setParameter(8, supportEmail)
      .setParameter(9, keywords)
      .setParameter(10, category.id)
      .setParameter(11, contentRating.id)
      .setParameter(12, priceTier.id)
      .setParameter(13, previousVersionID == 0 ? null : previousVersionID)
      .registerOut(14, Types.INTEGER);

    Ebean.execute(cs);

    return (Integer) cs.getObject(14);
  }

  public static void submit(int applicationID, int developerID) {
    CallableSql cs = Ebean.createCallableSql("{call " + DB_SCHEMA + ".f_application_submit(?, ?)}")
      .addModification(DB_SCHEMA + ".application", false, true, false)
      .addModification(DB_SCHEMA + ".application_event", true, false, false)
      .addModification(DB_SCHEMA + ".submission", true, false, false)
      .addModification(DB_SCHEMA + ".submission_event", true, false, false)
      .setParameter(1, applicationID)
      .setParameter(2, developerID);

    Ebean.execute(cs);
  }

  public boolean hasNextVersion() {
    CallableSql cs = Ebean.createCallableSql("{call " + DB_SCHEMA + ".f_application_has_next_version(?, ?)}")
      .setParameter(1, id)
      .registerOut(2, Types.BOOLEAN);

    Ebean.execute(cs);

    return (Boolean) cs.getObject(2);
  }

  public static boolean isOwner(int applicationID, int developerID) {
    CallableSql cs = Ebean.createCallableSql("{call " + DB_SCHEMA + ".f_application_is_owner(?, ?, ?)}")
      .setParameter(1, applicationID)
      .setParameter(2, developerID)
      .registerOut(3, Types.BOOLEAN);

    Ebean.execute(cs);

    return (Boolean) cs.getObject(3);
  }

  public static Boolean isNameTaken(String name) {
    CallableSql cs = Ebean.createCallableSql("{call " + DB_SCHEMA + ".f_application_is_name_taken(?, ?)}")
      .setParameter(1, name)
      .registerOut(2, Types.BOOLEAN);

    Ebean.execute(cs);

    return (Boolean) cs.getObject(2);
  }

  public static String findName(int applicationID) {
    CallableSql cs = Ebean.createCallableSql("{call " + DB_SCHEMA + ".f_application_find_name(?, ?)}")
      .setParameter(1, applicationID)
      .registerOut(2, Types.VARCHAR);

    Ebean.execute(cs);

    return (String) cs.getObject(2);
  }

  public static String findKeywords(int applicationID) {
    CallableSql cs = Ebean.createCallableSql("{call " + DB_SCHEMA + ".f_keyword_list_as_string(?, ?)}")
      .setParameter(1, applicationID)
      .registerOut(2, Types.VARCHAR);

    Ebean.execute(cs);

    return (String) cs.getObject(2);
  }

  public static int findUnsubmitted(int developerID) {
    CallableSql cs = Ebean.createCallableSql("{call " + DB_SCHEMA + ".f_application_find_unsubmitted_id(?, ?)}")
      .setParameter(1, developerID)
      .registerOut(2, Types.INTEGER);

    Ebean.execute(cs);

    return (Integer) cs.getObject(2);
  }

  public static Page page(int developerID, int index, int size, String sortBy, String order) throws SQLException {
    if (index < 1) index = 1;
    List<Application> applications = list(developerID, index, size, sortBy, order);

    return new Page(applications, index, size, totalRowCount(developerID));
  }

  private static List<Application> list(int developerID, int pageIndex, int pageSize, String sortBy, String order) {
    RawSql rawSql = RawSqlBuilder.unparsed("SELECT * FROM " + DB_SCHEMA + ".f_application_list_for_developer(p_developer_id:= ?, p_first_row := ?, p_max_rows := ?, p_sort_by := ?, p_order := ?)")
      .columnMapping("id", "id")
      .columnMapping("name", "name")
      .columnMapping("version", "version")
      .columnMapping("version_display", "versionDisplay")
      .columnMapping("category_id", "category.id")
      .columnMapping("category_name", "category.name")
      .columnMapping("state_id", "state.id")
      .columnMapping("state_name", "state.name")
      .create();

    return Ebean.find(Application.class)
      .setRawSql(rawSql)
      .setParameter(1, developerID)
      .setParameter(2, (pageIndex - 1) * pageSize)
      .setParameter(3, pageSize)
      .setParameter(4, sortBy)
      .setParameter(5, order)
      .findList();
  }

  private static long totalRowCount(int developerID) {
    CallableSql cs = Ebean.createCallableSql("{call " + DB_SCHEMA + ".f_application_find_count(?, ?)}")
      .setParameter(1, developerID)
      .registerOut(2, Types.BIGINT);

    Ebean.execute(cs);

    return (Long) cs.getObject(2);
  }

  public static class Page {
    public List<Application> list;
    public int index;
    public int size;
    public long totalRowCount;

    public Page(List<Application> list, int index, int size, long totalRowCount) {
      this.list = list;
      this.index = index;
      this.size = size;
      this.totalRowCount = totalRowCount;
    }

    public boolean hasPrev() {
      return index > 1;
    }

    public boolean hasNext() {
      return ((float) totalRowCount / size) > index;
    }

    public String displayXtoYofZ() {
      int start = ((index - 1) * size + 1);
      int end = start + Math.min(size, list.size()) - 1;
      return start + " to " + end + " of " + totalRowCount;
    }
  }
}
