package models;

import com.avaje.ebean.CallableSql;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.sql.Types;

import static controllers.Configuration.DB_SCHEMA;

@Entity
@Sql
public class Developer {
  public String name;
  public String email;

  public static Integer authenticate(String email, String passphrase) {
    CallableSql cs = Ebean.createCallableSql("{call " + DB_SCHEMA + ".f_developer_authenticate(?, ?, ?)}")
      .setParameter(1, email)
      .setParameter(2, passphrase)
      .registerOut(3, Types.INTEGER);

    Ebean.execute(cs);

    return (Integer) cs.getObject(3);
  }

  public static Developer find(int developerID) {
    RawSql rawSql = RawSqlBuilder.unparsed("SELECT * FROM " + DB_SCHEMA + ".f_developer_find(p_developer_id := ?)")
      .columnMapping("name", "name")
      .columnMapping("email", "email")
      .create();

    return Ebean.find(Developer.class)
      .setRawSql(rawSql)
      .setParameter(1, developerID)
      .findUnique();
  }
}
