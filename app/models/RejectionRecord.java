package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.annotation.Sql;
import models.classifiers.RejectionCriterion;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.List;

import static controllers.Configuration.DB_SCHEMA;

@Entity
@Sql
public class RejectionRecord {
  public String comment;
  @ManyToOne
  public RejectionCriterion criterion;

  public static List<RejectionRecord> list(int submissionID) {
    RawSql rawSql = RawSqlBuilder.unparsed("SELECT * FROM " + DB_SCHEMA + ".f_rejection_record_list(p_submission_id:= ?)")
      .columnMapping("comment", "comment")
      .columnMapping("crit_id", "criterion.id")
      .columnMapping("crit_name", "criterion.name")
      .columnMapping("crit_desc", "criterion.description")
      .create();

    return Ebean.find(RejectionRecord.class)
      .setRawSql(rawSql)
      .setParameter(1, submissionID)
      .findList();
  }

  @Override
  public int hashCode() {
    return 31 * comment.hashCode() + criterion.hashCode();
  }
}
