package models.classifiers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Maps.newLinkedHashMap;
import static controllers.Configuration.DB_SCHEMA;
import static play.data.validation.Constraints.Required;

@Entity
@Sql
public class PriceTier {
  @Id @Required
  public Integer id;
  public String name;
  public BigDecimal usd;
  public BigDecimal eur;

  public static Map<String, String> options() {
    LinkedHashMap<String, String> options = newLinkedHashMap();
    for (PriceTier t : list()) {
      options.put(t.id.toString(), t.name);
    }
    return options;
  }

  public static List<PriceTier> list() {
    RawSql rawSql = RawSqlBuilder.unparsed("SELECT * FROM " + DB_SCHEMA + ".f_price_tier_list()")
      .columnMapping("tier_id", "id")
      .columnMapping("tier_name", "name")
      .columnMapping("usd", "usd")
      .columnMapping("eur", "eur")
      .create();

    return Ebean.find(PriceTier.class)
      .setRawSql(rawSql)
      .findList();
  }
}
