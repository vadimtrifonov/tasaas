package models.classifiers;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Sql
public class SubmissionState {
  public static final int REJECTED = 3;

  @Id
  public Integer id;
  public String name;
}
