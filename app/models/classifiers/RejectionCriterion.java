package models.classifiers;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Sql
public class RejectionCriterion {
  @Id
  public int id;
  public String name;
  public String description;
}
