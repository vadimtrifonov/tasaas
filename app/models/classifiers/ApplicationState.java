package models.classifiers;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Sql
public class ApplicationState {
  public static final int NOT_SUBMITTED = 1;
  public static final int READY_FOR_SALE = 4;
  public static final int ON_SALE = 5;

  @Id
  public Integer id;
  public String name;
}