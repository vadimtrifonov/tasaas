package models.classifiers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Maps.newLinkedHashMap;
import static controllers.Configuration.DB_SCHEMA;
import static play.data.validation.Constraints.Required;

@Entity
@Sql
public class ContentRating {
  @Id @Required
  public Integer id;
  public String name;
  public String description;

  public static Map<String, String> options() {
    LinkedHashMap<String, String> options = newLinkedHashMap();
    for (ContentRating r : list()) {
      options.put(r.id.toString(), r.name);
    }
    return options;
  }

  public static List<ContentRating> list() {
    RawSql rawSql = RawSqlBuilder.unparsed("SELECT * FROM " + DB_SCHEMA + ".f_content_rating_list()")
      .columnMapping("rating_id", "id")
      .columnMapping("rating_name", "name")
      .columnMapping("rating_desc", "description")
      .create();

    return Ebean.find(ContentRating.class)
      .setRawSql(rawSql)
      .findList();
  }
}
