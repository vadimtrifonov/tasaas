package models.classifiers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Maps.newLinkedHashMap;
import static controllers.Configuration.DB_SCHEMA;
import static play.data.validation.Constraints.Required;

@Entity
@Sql
public class Category {
  @Id @Required
  public Integer id;
  public String name;

  public static Map<String, String> options() {
    LinkedHashMap<String, String> options = newLinkedHashMap();
    for (Category c : list()) {
      options.put(c.id.toString(), c.name);
    }
    return options;
  }

  public static List<Category> list() {
    RawSql rawSql = RawSqlBuilder.unparsed("SELECT * FROM " + DB_SCHEMA + ".f_category_list()")
      .columnMapping("cat_id", "id")
      .columnMapping("cat_name", "name")
      .create();

    return Ebean.find(Category.class)
      .setRawSql(rawSql)
      .findList();
  }
}
