$('input[id="name"], input[id="versionDisplay"], input[id="website"], input[id="supportEmail"], textarea[id="keywords"]').focusout(function() {
   var text = $.trim($(this).val());
   $(this).val(text);
});

$('input[id="name"], textarea[id="keywords"]').focusout(function() {
   var text = $(this).val().replace(/\s{2,}/g, ' ');
   $(this).val(text);
});

$('textarea[id="keywords"]').focusout(function() {
   var text = $(this).val().toLowerCase();
   $(this).val(text);
});
